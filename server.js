var connect = require('connect');
var superstatic = require('superstatic');

var app = connect();

app.use(superstatic({
  config: {
    root: "./dist",
    clean_urls: true,
    routes: {
      "**": "index.html"
    }
  }
}));

app.listen(process.env.PORT || 8004);
