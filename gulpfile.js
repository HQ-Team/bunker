  var gulp = require('gulp'),
	gulp_concat = require('gulp-concat'),
	gutil = require('gulp-util'),
	stylus = require('gulp-stylus'),
	nib = require('nib'),
	livereload = require('gulp-livereload');

gulp.task('pages', function() {
  gulp.src('dev/pages/*.html').pipe(gulp.dest('dist/pages')).pipe(livereload());
  gulp.src('dev/partials/*.html').pipe(gulp.dest('dist/partials')).pipe(livereload());
  gulp.src('dev/index.html').pipe(gulp.dest('dist/'));
});

gulp.task('styles', function(){
	gulp.src('dev/styles/styles.styl')
	.pipe(stylus({'include css':true,use:nib(),compress:true}))
	.pipe(gulp.dest('dist/css'))
	.pipe(livereload());
});

gulp.task('fonts', function(){
	gulp.src('dev/fonts/*')
	.pipe(gulp.dest('dist/fonts'))
	.pipe(livereload());
});

gulp.task('videos', function(){
	gulp.src('dev/videos/*')
	.pipe(gulp.dest('dist/videos'))
	.pipe(livereload());
});

gulp.task('images', function(){
	gulp.src('dev/images/*')
	.pipe(gulp.dest('dist/images'))
	.pipe(livereload());
});

gulp.task('scripts', function(){
	return gulp.src(
		['bower_components/jquery/dist/jquery.min.js',
		'dev/libs/bootstrap/bootstrap.min.js'])
  	.pipe(gulp_concat('app.js'))
  	.pipe(gulp.dest('dist/'))
		.pipe(livereload());
})

gulp.task('watch', function(){
	livereload.listen();
	gulp.watch('dev/styles/**/*.styl', ['styles']);
	gulp.watch('dev/scripts/**/*.js', ['scripts']);
	gulp.watch('dev/scripts/*.js', ['scripts']);
	gulp.watch('dev/pages/*.html', ['pages']);
	gulp.watch('dev/index.html', ['pages']);
  gulp.watch('dev/*.html', ['pages']);
	gulp.watch('dev/partials/*.html', ['pages']);
  gulp.watch('dev/images/*', ['images']);
})


gulp.task('default', ['pages','styles','fonts','videos','images','scripts','watch']);
